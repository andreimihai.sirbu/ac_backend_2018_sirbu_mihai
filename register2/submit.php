<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$sex = "";
$facultate = "";
$phone = "";
$email = "";
$cnp = "";
$facebook = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error=0;
$errortext="";
$result=0;

if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}

if(empty($firstname) || empty($lastname) || empty($facultate) || empty($phone) || empty($email) || empty($cnp) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check))
{
	$error = 1;
	echo "Toate campurile sunt obligatorii!";
}

if(strlen($firstname)<3 || strlen($firstname)>20)
{
	$error=1;
	echo "Lungimea prenumelui trebuie sa fie cuprinsa intre 3 si 20 de caractere";

}

if(strlen($lastname)<3 || strlen($lastname)>20)
{
	$error=1;
	echo "Lungimea numelui trebuie sa fie cuprinsa intre 3 si 20 de caractere";

}

if(strlen($facultate)<3 || strlen($facultate)>30)
{
	$error=1;
	echo "Lungimea numelui facultatii trebuie sa fie cuprinsa intre 3 si 30 de caractere";

}

if(strlen($question)<15)
{
	$error=1;
	echo "Lungimea raspunsului trebuie sa aiba minim 15 caractere";
}

if(!ctype_alpha($firstname))
{
	$error = 1;
	echo "Prenume invalid";

}

if(!ctype_alpha($lastname))
{
	$error = 1;
	echo "Nume invalid";

}

if(!ctype_alpha($facultate))
{
	$error = 1;
	echo "Facultate invalida";

}

if(!is_numeric($_POST['phone']) || strlen($_POST['phone'])!=10)
{
	$error = 1;
	echo "Nr telefon invalid";
}

if(!filter_var($email, FILTER_VALIDATE_EMAIL))
{
	$error = 1;
	echo "E-mail invalid";
}

if(strlen($_POST['cnp'])!=13 || $cnp[0]>6)
{
	$error = 1;
	echo "CNP invalid";
}

if(!filter_var($facebook, FILTER_VALIDATE_URL))
{
	$error = 1;
	echo "URL invalid";
}

function varsta(&$birth)
{
	$date = new DateTime($birth);
 	$now = new DateTime();
 	$interval = $now->diff($date);
 	return $interval->y;
}
 
 if(varsta($birth)<18 || varsta($birth) >100)
 {
 	$error = 1;
 	echo "Varsta nepotrivita";
 }

if($captcha_inserted!=$captcha_generated)
{
	$error = 1;
	echo "Captcha invalid";
}

if($cnp[0]==1 || $cnp[0]==3 || $cnp[0]==5)
{
	$sex="M";
}

if($cnp[0]==2 || $cnp[0]==4 || $cnp[0]==6)
{
	$sex="F";
}


//nu imi recunoaste functiile cu mysql_
/*function duplicates(&$email)
{
	$result = mysql_query("SELECT COUNT(email)
	FROM register2
	WHERE email like $email;");
	return $result;
}

if(duplicates($email)>0)
{
	$error = 1;
	echo "E-mail deja inscris";
}
*/


try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}
if(!$error)
{
	$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname, sex, facultate, phone,email,cnp,facebook,birth,department,question) VALUES(:firstname,:lastname,:sex,:facultate,:phone,:email,:cnp,:facebook,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':sex',$sex);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}	
}



